package com.bubna.transfer.money.controller

import io.netty.handler.codec.http.FullHttpRequest
import io.netty.handler.codec.http.FullHttpResponse

interface ControllerHandler {

    fun handle(request: FullHttpRequest): FullHttpResponse

}
package com.bubna.transfer.money.controller

import com.bubna.transfer.money.service.MoneyService
import com.bubna.transfer.money.utils.ex.impl.NotFoundException
import com.bubna.transfer.money.utils.toFullHttpResponse
import com.bubna.transfer.money.utils.toTransfer
import com.google.gson.Gson
import com.google.inject.Inject
import io.netty.handler.codec.http.*
import io.netty.handler.codec.http.router.Router

class MoneyController @Inject constructor(
    private val transferService: MoneyService,
    private val gson: Gson,
    private val next: ControllerHandler?
): ControllerHandler {

    private val router: Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse> = Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse>()
        .GET("/transfers") { r: FullHttpRequest, _: Map<String, String> -> getAll(r) }
        .GET("/transfers/:userId") { r: FullHttpRequest, pathParams: Map<String, String> -> get(pathParams.getValue("userId").toLong(), r) }
        .POST("/transfers") { r: FullHttpRequest, _: Map<String, String> -> transfer(r) }
        .notFound { r: FullHttpRequest, _: Map<String, String> -> next?.handle(r) ?: throw NotFoundException(
            HttpResponseStatus.NOT_FOUND,
            "path not found"
        )
        }

    override fun handle(request: FullHttpRequest): FullHttpResponse {
        val route = router.route(request.method(), request.uri())
        return route.target()(request, route.pathParams())
    }

    /*
    method - GET
     */
    fun getAll(request: FullHttpRequest): FullHttpResponse = transferService.getAll().toFullHttpResponse(gson, request)

    /*
    method - GET
     */
    fun get(id: Long, request: FullHttpRequest): FullHttpResponse =
        transferService.get(id).toFullHttpResponse(gson, request)

    /*
    method - POST
     */
    fun transfer(request: FullHttpRequest): FullHttpResponse {
        transferService.transfer(request.toTransfer(gson))
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.OK)
    }
}
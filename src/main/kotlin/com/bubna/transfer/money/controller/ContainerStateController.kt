package com.bubna.transfer.money.controller

import com.bubna.transfer.money.utils.ex.impl.NotFoundException
import com.google.inject.Inject
import io.netty.handler.codec.http.*
import io.netty.handler.codec.http.router.Router

class ContainerStateController @Inject constructor(
    private val next: ControllerHandler?
): ControllerHandler {

    private val router: Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse> = Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse>()
        .GET("/health-check") { r: FullHttpRequest, _: Map<String, String> -> DefaultFullHttpResponse(r.protocolVersion(), HttpResponseStatus.OK) }
        .notFound { r: FullHttpRequest, _: Map<String, String> -> next?.handle(r) ?: throw NotFoundException(
            HttpResponseStatus.NOT_FOUND,
            "path not found"
        )
        }

    override fun handle(request: FullHttpRequest): FullHttpResponse {
        val route = router.route(request.method(), request.uri())
        return route.target()(request, route.pathParams())
    }
}
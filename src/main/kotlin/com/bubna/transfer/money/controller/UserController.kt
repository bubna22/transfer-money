package com.bubna.transfer.money.controller

import com.bubna.transfer.money.service.AccountService
import com.bubna.transfer.money.service.UserService
import com.bubna.transfer.money.utils.ex.impl.NotFoundException
import com.bubna.transfer.money.utils.toAccount
import com.bubna.transfer.money.utils.toFullHttpResponse
import com.bubna.transfer.money.utils.toUser
import com.google.gson.Gson
import com.google.inject.Inject
import io.netty.handler.codec.http.DefaultFullHttpResponse
import io.netty.handler.codec.http.FullHttpRequest
import io.netty.handler.codec.http.FullHttpResponse
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.handler.codec.http.router.Router

class UserController @Inject constructor(
    private val userService: UserService,
    private val accountService: AccountService,
    private val gson: Gson,
    private val next: ControllerHandler?
) : ControllerHandler {

    private val router: Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse> =
        Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse>()
            .GET("/users") { r: FullHttpRequest, _: Map<String, String> -> getAll(r) }
            .GET("/users/:userId") { r: FullHttpRequest, pathParams: Map<String, String> ->
                get(
                    pathParams.getValue("userId").toLong(),
                    r
                )
            }
            .POST("/users") { r: FullHttpRequest, _: Map<String, String> -> create(r) }
            .POST("/users/:userId/accounts") { r: FullHttpRequest, pathParams: Map<String, String> ->
                createAccountForUser(
                    pathParams.getValue("userId").toLong(),
                    r
                )
            }
            .PUT("/users/:userId") { r: FullHttpRequest, pathParams: Map<String, String> ->
                update(
                    pathParams.getValue("userId").toLong(),
                    r
                )
            }
            .DELETE("/users/:userId") { r: FullHttpRequest, pathParams: Map<String, String> ->
                delete(
                    pathParams.getValue("userId").toLong(),
                    r
                )
            }
            .notFound { r: FullHttpRequest, _: Map<String, String> ->
                next?.handle(r) ?: throw NotFoundException(HttpResponseStatus.NOT_FOUND, "path not found")
            }

    override fun handle(request: FullHttpRequest): FullHttpResponse {
        val route = router.route(request.method(), request.uri())
        return route.target()(request, route.pathParams())
    }

    /*
    method - GET
    return all users
     */
    fun getAll(request: FullHttpRequest): FullHttpResponse = userService.getAll().toFullHttpResponse(gson, request)

    /*
    method - GET
    path param - user userId
    return user with requested userId
     */
    fun get(id: Long, request: FullHttpRequest): FullHttpResponse =
        userService.get(id).toFullHttpResponse(gson, request)

    /*
    method - POST
    make new user with received data
     */
    fun create(request: FullHttpRequest): FullHttpResponse {
        userService.create(request.toUser(gson))
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.CREATED)
    }

    /*
    method - POST
    make new user with received data
     */
    fun createAccountForUser(userId: Long, request: FullHttpRequest): FullHttpResponse {
        accountService.create(userId, request.toAccount(gson))
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.CREATED)
    }

    /*
    method - PUT
    update user with received data
     */
    fun update(id: Long, request: FullHttpRequest): FullHttpResponse {
        userService.update(request.toUser(gson).withId(id))
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.OK)
    }

    /*
    method - DELETE
    delete user with received data
     */
    fun delete(id: Long, request: FullHttpRequest): FullHttpResponse {
        userService.delete(id)
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.OK)
    }
}
package com.bubna.transfer.money.controller

import com.bubna.transfer.money.service.AccountService
import com.bubna.transfer.money.utils.ex.impl.NotFoundException
import com.bubna.transfer.money.utils.toAccount
import com.bubna.transfer.money.utils.toFullHttpResponse
import com.google.gson.Gson
import com.google.inject.Inject
import io.netty.handler.codec.http.*
import io.netty.handler.codec.http.router.Router

class AccountController @Inject constructor(
    private val accountService: AccountService,
    private val gson: Gson,
    private val next: ControllerHandler?
): ControllerHandler {

    private val router: Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse> = Router<(request: FullHttpRequest, pathParams: Map<String, String>) -> FullHttpResponse>()
        .GET("/accounts") { r: FullHttpRequest, _: Map<String, String> -> getAll(r) }
        .GET("/accounts/:userId") { r: FullHttpRequest, pathParams: Map<String, String> -> get(pathParams.getValue("userId"), r) }
        .PUT("/accounts/:userId") { r: FullHttpRequest, pathParams: Map<String, String> -> update(pathParams.getValue("userId"), r) }
        .DELETE("/accounts/:userId") { r: FullHttpRequest, pathParams: Map<String, String> -> delete(pathParams.getValue("userId"), r) }
        .notFound { r: FullHttpRequest, _: Map<String, String> -> next?.handle(r) ?: throw NotFoundException(
            HttpResponseStatus.NOT_FOUND,
            "path not found"
        )
        }

    override fun handle(request: FullHttpRequest): FullHttpResponse {
        val route = router.route(request.method(), request.uri())
        return route.target()(request, route.pathParams())
    }

    /*
    method - GET
    return all users
     */
    fun getAll(request: FullHttpRequest): FullHttpResponse = accountService.findAll().toFullHttpResponse(gson, request)

    /*
    method - GET
    path param - user userId
    return user with requested userId
     */
    fun get(id: String, request: FullHttpRequest): FullHttpResponse =
        accountService.find(id).toFullHttpResponse(gson, request)

    /*
    method - PUT
    update user with received data
     */
    fun update(accNumber: String, request: FullHttpRequest): FullHttpResponse {
        accountService.update(request.toAccount(gson).withAccountNumber(accNumber))
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.OK)
    }

    /*
    method - DELETE
    delete user with received data
     */
    fun delete(accNumber: String, request: FullHttpRequest): FullHttpResponse {
        accountService.delete(accNumber)
        return DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.OK)
    }
}
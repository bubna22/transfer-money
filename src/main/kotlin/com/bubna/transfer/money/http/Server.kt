package com.bubna.transfer.money.http

import com.bubna.transfer.money.controller.ControllerHandler
import com.bubna.transfer.money.guice.HttpUserController
import com.bubna.transfer.money.guice.TransferModule
import com.bubna.transfer.money.utils.ex.IException
import com.bubna.transfer.money.utils.toFullHttpResponse
import com.google.gson.Gson
import com.google.inject.Inject
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.http.*
import com.google.inject.Guice
import com.google.inject.Injector
import org.slf4j.LoggerFactory
import java.util.concurrent.CompletableFuture


class Server {
    private val log = LoggerFactory.getLogger(Server::class.java)

    fun run(port: Int) {
        val injector = Guice.createInjector(TransferModule())
        val bossGroup = NioEventLoopGroup() // (1)
        val workerGroup = NioEventLoopGroup()
        try {
            val b = ServerBootstrap() // (2)
            b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel::class.java) // (3)
                .childHandler(injector.getInstance(ChannelHttpInitializer::class.java))
                .option(ChannelOption.SO_BACKLOG, 128)          // (5)
                .childOption(ChannelOption.SO_KEEPALIVE, true) // (6)

            // Bind and start to accept incoming connections.
            val f = b.bind(port).sync() // (7)
            log.debug("server started")

            f.channel().closeFuture().sync()
        } finally {
            workerGroup.shutdownGracefully()
            bossGroup.shutdownGracefully()
        }
    }
}

class ChannelHttpInitializer @Inject constructor(private val injector: Injector) : ChannelInitializer<SocketChannel>() {

    override fun initChannel(ch: SocketChannel) {
        ch.pipeline().addLast(
            HttpServerCodec(),
            HttpObjectAggregator(1048576),
            injector.getInstance(HttpHandler::class.java)
        )
    }
}

class HttpHandler @Inject constructor(
                  private val gson: Gson,
                  @HttpUserController private val controllerChain: ControllerHandler
) : ChannelInboundHandlerAdapter() {

    @Throws(Exception::class)
    override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
        val response = controllerChain.handle(msg as FullHttpRequest)
        response.headers().add(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE)
    }

    @Throws(Exception::class)
    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {

        val response = if (cause is IException)
            cause.toFullHttpResponse(gson, HttpVersion.HTTP_1_1, cause.getHttpStatus())
        else
            cause.toFullHttpResponse(gson, HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR)
        response.headers().add(HttpHeaderNames.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE)
    }
}
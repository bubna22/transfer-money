package com.bubna.transfer.money.model

class UserDto(val userId: Long?, val fName: String, val sName: String, accountsList: ArrayList<String>?) {
    val accounts: ArrayList<String> = accountsList ?: ArrayList()

    fun withId(id: Long) = UserDto(id, fName, sName, accounts)

    fun addAccountNumber(accNumber: String): UserDto {
        accounts.add(accNumber)
        return this
    }
}
package com.bubna.transfer.money.model

import java.math.BigDecimal

data class AccountDto(val accNumber: String?, val accBalance: BigDecimal) {
    fun withAccountNumber(accNumber: String) = AccountDto(accNumber, accBalance)

    fun withBalance(accBalance: BigDecimal) = AccountDto(accNumber, accBalance)
}
package com.bubna.transfer.money.model

import java.math.BigDecimal

data class TransferDto(val transferId: Long?, val fr: String, val to: String, val amount: BigDecimal) {
    fun withId(transferId: Long) = TransferDto(transferId, fr, to, amount)
}
package com.bubna.transfer.money

import com.bubna.transfer.money.http.Server

fun main() {
    val port = 8080

    Server().run(port)
}
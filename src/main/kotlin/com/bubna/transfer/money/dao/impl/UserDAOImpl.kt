package com.bubna.transfer.money.dao.impl

import com.bubna.jooq.public_.Tables.*
import com.bubna.transfer.money.dao.UserDAO
import com.bubna.transfer.money.model.AccountDto
import com.bubna.transfer.money.model.UserDto
import com.bubna.transfer.money.utils.dsl.DSLContextAgregator
import com.google.inject.Inject
import com.google.inject.Injector
import com.google.inject.Singleton
import org.jooq.Configuration
import org.jooq.impl.DSL
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.collections.HashMap

@Singleton
class UserDAOImpl @Inject constructor(
    private val injector: Injector
): UserDAO {

    private val log = LoggerFactory.getLogger(UserDAOImpl::class.java)

    override fun find(id: Long): UserDto {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            val res =
                dslAggregator.dsl.select(USERS.ID, USERS.FNAME, USERS.SNAME, USER_ACCOUNT.ACCOUNTNUMBER).from(USERS)
                    .leftJoin(USER_ACCOUNT).on(USER_ACCOUNT.USERID.eq(USERS.ID))
                    .where(USERS.ID.eq(id))
                    .fetch()

            val accounts = res.filter { it[USER_ACCOUNT.ACCOUNTNUMBER] != null }.map { it[USER_ACCOUNT.ACCOUNTNUMBER] }
            val userFields = res.first()
            return UserDto(userFields[USERS.ID], userFields[USERS.FNAME], userFields[USERS.SNAME], ArrayList(accounts))
        } finally {
            log.debug("find with args $id")
            dslAggregator.con.close()
        }
    }

    override fun findAll(): List<UserDto> {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            val records =
                dslAggregator.dsl.select(USERS.ID, USERS.FNAME, USERS.SNAME, USER_ACCOUNT.ACCOUNTNUMBER).from(USERS)
                    .leftJoin(USER_ACCOUNT).on(USER_ACCOUNT.USERID.eq(USERS.ID))
                    .fetch()

            val userMap = HashMap<Long, UserDto>()

            for (r in records) {
                val userId = r[USERS.ID]
                val accNum = r[USER_ACCOUNT.ACCOUNTNUMBER]

                if (userMap.containsKey(userId)) {
                    userMap[userId]!!.addAccountNumber(accNum)
                    continue
                }
                val fName = r[USERS.FNAME]
                val sName = r[USERS.SNAME]
                userMap[userId] =
                    if (accNum == null)
                        UserDto(userId, fName, sName, null)
                    else
                        UserDto(userId, fName, sName, null).addAccountNumber(accNum)
            }
            return userMap.values.toList()
        } finally {
            log.debug("findAll called")
            dslAggregator.con.close()
        }
    }

    override fun create(user: UserDto) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl.transaction { config: Configuration ->
                val dsl = DSL.using(config)
                dsl.insertInto(USERS, USERS.FNAME, USERS.SNAME).values(user.fName, user.sName).returning()
                    .fetchOne()
                // we can't create user with accounts
            }
        } finally {
            log.debug("create with args $user")
            dslAggregator.con.close()
        }
    }

    override fun createAcc(userId: Long, accountDto: AccountDto) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl.transaction { config: Configuration ->
                val dsl = DSL.using(config)
                val existing = dsl.select(USERS.ID).from(USERS)
                    .where(USERS.ID.eq(userId))
                    .fetchOne()
                Optional.ofNullable(existing.getValue(USERS.ID)).orElseThrow()
                val uuid = UUID.randomUUID().toString()
                dsl.insertInto(ACCOUNTS, ACCOUNTS.ACCOUNTNUMBER, ACCOUNTS.ACCOUNTBALANCE)
                        .values(uuid, accountDto.accBalance)
                        .returning().fetchOne()
                dsl.insertInto(USER_ACCOUNT, USER_ACCOUNT.USERID, ACCOUNTS.ACCOUNTNUMBER).values(userId, uuid).execute()
            }
        } finally {
            log.debug("createAcc with args $userId and $accountDto")
            dslAggregator.con.close()
        }
    }

    override fun update(user: UserDto) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl.transaction { config: Configuration ->
                val dsl = DSL.using(config)
                dsl.update(USERS)
                    .set(USERS.FNAME, user.fName)
                    .set(USERS.SNAME, user.sName)
                    .where(USERS.ID.eq(user.userId))
                    .execute()
                //we can't update user accounts here
            }
        } finally {
            log.debug("update with args $user")
            dslAggregator.con.close()
        }
    }

    override fun delete(userId: Long) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl.transaction { config: Configuration ->
                val dsl = DSL.using(config)

                val usersAccounts =
                    dsl.select(USER_ACCOUNT.ACCOUNTNUMBER)
                        .from(USER_ACCOUNT)
                        .where(USER_ACCOUNT.USERID.eq(userId))
                        .fetch()
                        .map { it[USER_ACCOUNT.ACCOUNTNUMBER] }


                dsl.delete(ACCOUNTS).where(ACCOUNTS.ACCOUNTNUMBER.`in`(usersAccounts)).execute()
                dsl.delete(USER_ACCOUNT).where(USER_ACCOUNT.ACCOUNTNUMBER.`in`(usersAccounts)).execute()
                dsl.delete(USERS).where(USERS.ID.eq(userId)).execute()
            }
        } finally {
            log.debug("delete with args $userId")
            dslAggregator.con.close()
        }
    }

}

package com.bubna.transfer.money.dao

import com.bubna.transfer.money.model.AccountDto

interface AccountDAO {
    fun find(accountNumber: String): AccountDto

    fun findAll(): List<AccountDto>

    fun update(accountDto: AccountDto)

    fun delete(accountNumber: String)
}
package com.bubna.transfer.money.dao.impl

import com.bubna.jooq.public_.Tables.TRANSFERS
import com.bubna.transfer.money.dao.MoneyDAO
import com.bubna.transfer.money.model.TransferDto
import com.bubna.transfer.money.utils.dsl.DSLContextAgregator
import com.google.inject.Inject
import com.google.inject.Injector
import com.google.inject.Singleton
import org.slf4j.LoggerFactory

@Singleton
class MoneyDAOImpl @Inject constructor(private val injector: Injector): MoneyDAO {

    private val log = LoggerFactory.getLogger(MoneyDAOImpl::class.java)

    override fun get(transferId: Long): TransferDto {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            return dslAggregator.dsl.select().from(TRANSFERS).where(TRANSFERS.ID.eq(transferId)).fetchOneInto(TransferDto::class.java)
        } finally {
            log.debug("get with args $transferId")
            dslAggregator.con.close()
        }
    }

    override fun getAll(): List<TransferDto> {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            return dslAggregator.dsl.select().from(TRANSFERS).fetchInto(TransferDto::class.java)
        } finally {
            log.debug("getAll called")
            dslAggregator.con.close()
        }
    }

    override fun add(transferDto: TransferDto) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl
                .insertInto(TRANSFERS, TRANSFERS.ID, TRANSFERS.FR, TRANSFERS.TO, TRANSFERS.AMOUNT)
                .values(transferDto.transferId, transferDto.fr, transferDto.to, transferDto.amount)
                .execute()
        } finally {
            log.debug("add with args $transferDto")
            dslAggregator.con.close()
        }
    }
}
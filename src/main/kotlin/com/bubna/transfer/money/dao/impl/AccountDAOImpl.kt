package com.bubna.transfer.money.dao.impl

import com.bubna.jooq.public_.Tables.*
import com.bubna.transfer.money.dao.AccountDAO
import com.bubna.transfer.money.model.AccountDto
import com.bubna.transfer.money.utils.dsl.DSLContextAgregator
import com.google.inject.Inject
import com.google.inject.Injector
import com.google.inject.Singleton
import org.jooq.Configuration
import org.jooq.impl.DSL
import org.slf4j.LoggerFactory

@Singleton
class AccountDAOImpl @Inject constructor(private val injector: Injector) : AccountDAO {

    private val log = LoggerFactory.getLogger(AccountDAOImpl::class.java)

    override fun find(accountNumber: String): AccountDto {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            return dslAggregator.dsl.select().from(ACCOUNTS).where(ACCOUNTS.ACCOUNTNUMBER.eq(accountNumber))
                .fetchOneInto(AccountDto::class.java)
        } finally {
            log.debug("find with args $accountNumber")
            dslAggregator.con.close()
        }
    }

    override fun findAll(): List<AccountDto> {
        log.debug("findAll called")
        return injector.getInstance(DSLContextAgregator::class.java).dsl
            .select().from(ACCOUNTS).fetchInto(AccountDto::class.java)
    }

    override fun update(accountDto: AccountDto) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl.transaction { config: Configuration ->
                DSL.using(config).update(ACCOUNTS).set(ACCOUNTS.ACCOUNTBALANCE, accountDto.accBalance)
                    .where(ACCOUNTS.ACCOUNTNUMBER.eq(accountDto.accNumber))
                    .execute()
            }
        } finally {
            log.debug("update with args $accountDto")
            dslAggregator.con.close()
        }
    }

    override fun delete(accountNumber: String) {
        val dslAggregator = injector.getInstance(DSLContextAgregator::class.java)
        try {
            dslAggregator.dsl.transaction { config: Configuration ->
                val dsl = DSL.using(config)

                dsl.delete(ACCOUNTS).where(ACCOUNTS.ACCOUNTNUMBER.eq(accountNumber)).execute()
                dsl.delete(USER_ACCOUNT).where(USER_ACCOUNT.ACCOUNTNUMBER.eq(accountNumber)).execute()
            }
        } finally {
            log.debug("delete with args $accountNumber")
            dslAggregator.con.close()
        }
    }
}


package com.bubna.transfer.money.dao

import com.bubna.transfer.money.model.AccountDto
import com.bubna.transfer.money.model.UserDto

interface UserDAO {
    fun find(id: Long): UserDto

    fun findAll(): List<UserDto>

    fun create(user: UserDto)

    fun createAcc(userId: Long, accountDto: AccountDto)

    fun update(user: UserDto)

    fun delete(userId: Long)
}
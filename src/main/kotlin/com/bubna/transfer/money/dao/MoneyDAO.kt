package com.bubna.transfer.money.dao

import com.bubna.transfer.money.model.TransferDto

interface MoneyDAO {

    fun get(transferId: Long): TransferDto

    fun getAll(): List<TransferDto>

    fun add(transferDto: TransferDto)
}
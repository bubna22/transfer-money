package com.bubna.transfer.money.utils.dsl

import bitronix.tm.BitronixTransactionManager
import com.google.inject.Inject
import org.jooq.TransactionContext
import org.jooq.TransactionProvider

class BitronixTransactionProvider @Inject constructor(
    private val tm: BitronixTransactionManager
): TransactionProvider {

    override fun begin(ctx: TransactionContext?) = tm.begin()

    override fun rollback(ctx: TransactionContext?) = tm.rollback()

    override fun commit(ctx: TransactionContext?) = tm.commit()

}
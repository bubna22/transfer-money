package com.bubna.transfer.money.utils.dsl

import com.google.inject.Inject
import org.jooq.DSLContext
import java.sql.Connection

data class DSLContextAgregator @Inject constructor(val dsl: DSLContext, val con: Connection)
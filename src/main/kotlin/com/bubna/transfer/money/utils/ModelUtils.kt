package com.bubna.transfer.money.utils

import com.bubna.transfer.money.model.AccountDto
import com.bubna.transfer.money.model.TransferDto
import com.bubna.transfer.money.model.UserDto
import com.google.gson.Gson
import io.netty.buffer.Unpooled
import io.netty.handler.codec.http.*
import java.nio.charset.Charset

fun FullHttpRequest.toUser(gson: Gson): UserDto {
    return gson.fromJson(this.content().toString(Charset.forName("UTF-8")), UserDto::class.java)
}

fun FullHttpRequest.toAccount(gson: Gson): AccountDto {
    return gson.fromJson(this.content().toString(Charset.forName("UTF-8")), AccountDto::class.java)
}

fun FullHttpRequest.toTransfer(gson: Gson): TransferDto {
    return gson.fromJson(this.content().toString(Charset.forName("UTF-8")), TransferDto::class.java)
}

fun Any.toFullHttpResponse(gson: Gson, request: FullHttpRequest): FullHttpResponse {
    return DefaultFullHttpResponse(
        request.protocolVersion(),
        HttpResponseStatus.OK,
        Unpooled.copiedBuffer(gson.toJson(this).toByteArray())
    )
}

fun List<*>.toFullHttpResponse(gson: Gson, request: FullHttpRequest): FullHttpResponse {
    return DefaultFullHttpResponse(
        request.protocolVersion(),
        if (this.isEmpty()) HttpResponseStatus.NO_CONTENT else HttpResponseStatus.OK,
        Unpooled.copiedBuffer(gson.toJson(this).toByteArray())
    )
}

fun Throwable.toFullHttpResponse(gson: Gson, protocolVersion: HttpVersion, responseStatus: HttpResponseStatus): FullHttpResponse {
    return DefaultFullHttpResponse(
        protocolVersion,
        responseStatus,
        Unpooled.copiedBuffer(gson.toJson(this).toByteArray())
    )
}
package com.bubna.transfer.money.utils.ex.impl

import com.bubna.transfer.money.utils.ex.IException
import io.netty.handler.codec.http.HttpResponseStatus
import java.math.BigDecimal

data class NegativeBalanceException(val status: HttpResponseStatus, val balance: BigDecimal, override val message: String): RuntimeException(message),
    IException {
    override fun getHttpStatus() = status
}
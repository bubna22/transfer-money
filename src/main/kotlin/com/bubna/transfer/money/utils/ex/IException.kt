package com.bubna.transfer.money.utils.ex

import io.netty.handler.codec.http.HttpResponseStatus

/**
 * signing interface
 */
interface IException {
    fun getHttpStatus(): HttpResponseStatus
}
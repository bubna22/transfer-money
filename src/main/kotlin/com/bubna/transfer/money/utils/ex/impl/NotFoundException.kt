package com.bubna.transfer.money.utils.ex.impl

import com.bubna.transfer.money.utils.ex.IException
import io.netty.handler.codec.http.HttpResponseStatus

data class NotFoundException(val status: HttpResponseStatus, override val message: String): RuntimeException(message),
    IException {
    override fun getHttpStatus() = status
}
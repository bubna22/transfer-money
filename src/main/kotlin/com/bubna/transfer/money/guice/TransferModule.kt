package com.bubna.transfer.money.guice

import bitronix.tm.TransactionManagerServices
import bitronix.tm.jndi.BitronixContext
import bitronix.tm.resource.jdbc.PoolingDataSource
import com.bubna.transfer.money.controller.*
import com.bubna.transfer.money.dao.AccountDAO
import com.bubna.transfer.money.dao.MoneyDAO
import com.bubna.transfer.money.dao.UserDAO
import com.bubna.transfer.money.dao.impl.AccountDAOImpl
import com.bubna.transfer.money.dao.impl.MoneyDAOImpl
import com.bubna.transfer.money.dao.impl.UserDAOImpl
import com.bubna.transfer.money.http.ChannelHttpInitializer
import com.bubna.transfer.money.http.HttpHandler
import com.bubna.transfer.money.service.AccountService
import com.bubna.transfer.money.service.MoneyService
import com.bubna.transfer.money.service.UserService
import com.bubna.transfer.money.service.impl.AccountServiceImpl
import com.bubna.transfer.money.service.impl.MoneyServiceImpl
import com.bubna.transfer.money.service.impl.UserServiceImpl
import com.bubna.transfer.money.utils.CollectionAdapter
import com.bubna.transfer.money.utils.dsl.BitronixTransactionProvider
import com.bubna.transfer.money.utils.dsl.DSLContextAgregator
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.inject.*
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import java.sql.Connection
import javax.sql.DataSource


class TransferModule : AbstractModule() {

    override fun configure() {
        dataSourceInit()

        bind(BitronixTransactionProvider::class.java)

        bind(AccountDAO::class.java).to(AccountDAOImpl::class.java)
        bind(MoneyDAO::class.java).to(MoneyDAOImpl::class.java)
        bind(UserDAO::class.java).to(UserDAOImpl::class.java)

        bind(AccountService::class.java).to(AccountServiceImpl::class.java)
        bind(MoneyService::class.java).to(MoneyServiceImpl::class.java)
        bind(UserService::class.java).to(UserServiceImpl::class.java)

        bind(HttpHandler::class.java)
        bind(ChannelHttpInitializer::class.java)

        bind(ControllerHandler::class.java).annotatedWith(HttpUserController::class.java)
            .toProvider(UserControllerProvider::class.java)
        bind(ControllerHandler::class.java).annotatedWith(HttpAccountController::class.java)
            .toProvider(AccountControllerProvider::class.java)
        bind(ControllerHandler::class.java).annotatedWith(HttpMoneyController::class.java)
            .toProvider(MoneyControllerProvider::class.java)
        bind(ControllerHandler::class.java).annotatedWith(HttpContainerStateController::class.java)
            .toProvider(ContainerStateControllerProvider::class.java)
    }

    @Provides
    fun gson() = GsonBuilder().registerTypeHierarchyAdapter(Collection::class.java, CollectionAdapter()).create()

    @Provides @Singleton
    fun dataSource() = BitronixContext().lookup("h2") as DataSource

    //for h2
    @Provides
    fun dsConnection(dataSource: DataSource) = dataSource.connection

    @Provides
    fun dsl(connection: Connection, tp: BitronixTransactionProvider): DSLContextAgregator {
        val dsl = DSL.using(connection, SQLDialect.H2)
        dsl.configuration().set(tp)
        return DSLContextAgregator(dsl, connection)
    }

    @Provides @Singleton
    fun tm() = TransactionManagerServices.getTransactionManager()

    fun dataSourceInit() {
        val dataSource = PoolingDataSource()
        dataSource.setClassName("org.h2.jdbcx.JdbcDataSource")
        dataSource.setUniqueName("h2")
        dataSource.setMaxPoolSize(50)
        dataSource.setAllowLocalTransactions(true)
        dataSource.getDriverProperties().setProperty("user", "test")
        dataSource.getDriverProperties().setProperty("password", "test")
        val initSqlPath = TransferModule::class.java.classLoader.resources("h2").findAny().orElseThrow().toString()
        dataSource.getDriverProperties().setProperty("URL", "jdbc:h2:mem:db;INIT=create schema if not exists PUBLIC\\;RUNSCRIPT FROM \\'${initSqlPath}/create-tables.sql\'")
        dataSource.init()
    }
}

class ContainerStateControllerProvider : Provider<ControllerHandler> {
    override fun get() = ContainerStateController(null)
}

class MoneyControllerProvider @Inject constructor(
    private val moneyService: MoneyService,
    private val gson: Gson,
    @HttpContainerStateController private val nextChain: ControllerHandler
) : Provider<ControllerHandler> {
    override fun get() = MoneyController(moneyService, gson, nextChain)
}

class AccountControllerProvider @Inject constructor(
    private val accountService: AccountService,
    private val gson: Gson,
    @HttpMoneyController private val nextChain: ControllerHandler
) : Provider<ControllerHandler> {
    override fun get() = AccountController(accountService, gson, nextChain)
}

class UserControllerProvider @Inject constructor(
    private val userService: UserService,
    private val accountService: AccountService,
    private val gson: Gson,
    @HttpAccountController private val nextChain: ControllerHandler
) : Provider<ControllerHandler> {
    override fun get() = UserController(userService, accountService, gson, nextChain)
}

@BindingAnnotation
@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class HttpUserController

@BindingAnnotation
@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class HttpMoneyController

@BindingAnnotation
@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class HttpContainerStateController

@BindingAnnotation
@Target(AnnotationTarget.FIELD, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class HttpAccountController
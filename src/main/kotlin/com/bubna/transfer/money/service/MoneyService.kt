package com.bubna.transfer.money.service

import com.bubna.transfer.money.model.TransferDto

interface MoneyService {

    fun get(id: Long): TransferDto

    fun getAll(): List<TransferDto>

    fun transfer(transferInfo: TransferDto)
}
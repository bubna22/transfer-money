package com.bubna.transfer.money.service.impl

import com.bubna.transfer.money.dao.MoneyDAO
import com.bubna.transfer.money.model.TransferDto
import com.bubna.transfer.money.service.AccountService
import com.bubna.transfer.money.service.MoneyService
import com.google.inject.Inject
import com.google.inject.Singleton

@Singleton
class MoneyServiceImpl @Inject constructor(
    private val moneyDao: MoneyDAO,
    private val accountService: AccountService
): MoneyService {
    override fun get(id: Long) = moneyDao.get(id)

    override fun getAll() = moneyDao.getAll()

    override fun transfer(transferInfo: TransferDto) {
        val accountFrom = accountService.find(transferInfo.fr)
        val accountTo = accountService.find(transferInfo.to)

        accountService.update(accountFrom.withBalance(accountFrom.accBalance - transferInfo.amount))
        accountService.update(accountTo.withBalance(accountTo.accBalance + transferInfo.amount))
        moneyDao.add(transferInfo)
    }

}
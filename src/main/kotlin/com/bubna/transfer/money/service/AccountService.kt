package com.bubna.transfer.money.service

import com.bubna.transfer.money.model.AccountDto
import com.bubna.transfer.money.model.UserDto

interface AccountService {
    fun find(accountNumber: String): AccountDto

    fun findAll(): List<AccountDto>

    fun create(userId: Long, accountDto: AccountDto)

    fun update(accountDto: AccountDto)

    fun delete(accountNumber: String)
}
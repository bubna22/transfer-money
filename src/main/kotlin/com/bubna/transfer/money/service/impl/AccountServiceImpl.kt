package com.bubna.transfer.money.service.impl

import com.bubna.transfer.money.dao.AccountDAO
import com.bubna.transfer.money.dao.UserDAO
import com.bubna.transfer.money.model.AccountDto
import com.bubna.transfer.money.service.AccountService
import com.bubna.transfer.money.utils.ex.impl.NegativeBalanceException
import com.google.inject.Inject
import com.google.inject.Singleton
import io.netty.handler.codec.http.HttpResponseStatus
import java.math.BigDecimal

@Singleton
class AccountServiceImpl @Inject constructor(
    private val accountDao: AccountDAO,
    private val userDao: UserDAO
): AccountService {

    override fun findAll() = accountDao.findAll()

    override fun create(userId: Long, accountDto: AccountDto) {
        if (accountDto.accBalance < BigDecimal.ZERO)
            throw NegativeBalanceException(HttpResponseStatus.UNPROCESSABLE_ENTITY, accountDto.accBalance, "balance of acc $accountDto is below zero")
        userDao.createAcc(userId, accountDto)
    }

    override fun update(accountDto: AccountDto) {
        if (accountDto.accBalance < BigDecimal.ZERO)
            throw NegativeBalanceException(HttpResponseStatus.UNPROCESSABLE_ENTITY, accountDto.accBalance, "balance of acc $accountDto is below zero")
        accountDao.update(accountDto)
    }

    override fun delete(accountNumber: String) {
        accountDao.delete(accountNumber)
    }

    override fun find(accountNumber: String) = accountDao.find(accountNumber)
}
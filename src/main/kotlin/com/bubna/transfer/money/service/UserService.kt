package com.bubna.transfer.money.service

import com.bubna.transfer.money.model.UserDto

interface UserService {

    fun get(id: Long): UserDto

    fun getAll(): List<UserDto>

    fun create(user: UserDto)

    fun update(user: UserDto)

    fun delete(id: Long)

}
package com.bubna.transfer.money.service.impl

import com.bubna.transfer.money.dao.AccountDAO
import com.bubna.transfer.money.dao.UserDAO
import com.bubna.transfer.money.model.UserDto
import com.bubna.transfer.money.service.UserService
import com.google.inject.Inject
import com.google.inject.Singleton

@Singleton
class UserServiceImpl @Inject constructor(
    private val accountDao: AccountDAO,
    private val userDao: UserDAO
): UserService {
    override fun get(id: Long) = userDao.find(id)

    override fun getAll() = userDao.findAll()

    override fun create(user: UserDto) = userDao.create(user)

    override fun update(user: UserDto) = userDao.update(user)

    override fun delete(id: Long) {
        userDao.find(id).accounts.forEach { accountDao.delete(it) }
        userDao.delete(id)
    }
}
package com.bubna.transfer.money.controller.integration

import com.bubna.transfer.money.BaseTestSpec
import io.restassured.path.json.config.JsonPathConfig
import org.jooq.tools.json.JSONObject

import static io.restassured.RestAssured.*
import static io.restassured.config.JsonConfig.jsonConfig
import static io.restassured.config.RestAssuredConfig.newConfig
import static org.hamcrest.Matchers.comparesEqualTo
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.notNullValue

class HttpUserControllerSpec extends BaseTestSpec {

    def setup() {
        config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))
    }

    def "/users returns 1 user"() {
        setup:
        JSONObject requestBody = new JSONObject()
        requestBody.put("fName", "ilia")
        requestBody.put("sName", "yavorskiy")

        given().request().body(requestBody.toString()).post("/users").statusCode() == 201
        expect:
        when()
                .get("/users")
        .then()
                .statusCode(200)
                .body("[0].fName", equalTo("ilia"))
                .body("[0].sName", equalTo("yavorskiy"))
        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/users/{id} returns concrete user"() {
        setup:
        JSONObject requestBody = new JSONObject()
        requestBody.put("fName", "ilia")
        requestBody.put("sName", "yavorskiy")

        given().request().body(requestBody.toString()).post("/users").statusCode() == 201
        expect:
        when()
                .get("/users/{userId}", get("/users").body().jsonPath().get("[0].userId"))
                .then()
                .statusCode(200)
                .body("fName", equalTo("ilia"))
                .body("sName", equalTo("yavorskiy"))
        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/users/{id} updates successfully"() {
        setup:
        JSONObject requestBody = new JSONObject()
        requestBody.put("fName", "ilia")
        requestBody.put("sName", "yavorskiy")

        given().request().body(requestBody.toString()).post("/users").statusCode() == 201

        requestBody.put("fName", "ilya")
        expect:
        given()
                .request()
                .body(requestBody.toString())
                .put("/users/{userId}", get("/users").body().jsonPath().get("[0].userId"))
                .then()
                .statusCode(200)

        when()
                .get("/users/{userId}", get("/users").body().jsonPath().get("[0].userId"))
        .then()
                .statusCode(200)
                .body("fName", equalTo("ilya"))
                .body("sName", equalTo("yavorskiy"))
        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/users/{id} deletes successfully"() {
        setup:
        JSONObject requestBody = new JSONObject()
        requestBody.put("fName", "ilia")
        requestBody.put("sName", "yavorskiy")

        given().request().body(requestBody.toString()).post("/users").statusCode() == 201
        expect:
        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
        when()
                .get("/users")
        .then()
                .statusCode(204)
    }

    def "/users/{id}/accounts creates account for concrete user"() {
        setup:
        JSONObject requestUserBody = new JSONObject()
        requestUserBody.put("fName", "ilia")
        requestUserBody.put("sName", "yavorskiy")
        given().request().body(requestUserBody.toString()).post("/users")

        JSONObject requestAccBody = new JSONObject()
        requestAccBody.put("accBalance", "0.0")
        expect:
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", get("/users").body().jsonPath().get("[0].userId"))
        .then()
                .statusCode(201)
        when()
                .get("/users/{userId}", get("/users").body().jsonPath().get("[0].userId"))
        .then()
                .statusCode(200)
                .body("fName", equalTo("ilia"))
                .body("sName", equalTo("yavorskiy"))
                .body("accounts", notNullValue())
        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/users/{id}/accounts prevents creation with negative balance"() {
        setup:
        JSONObject requestUserBody = new JSONObject()
        requestUserBody.put("fName", "ilia")
        requestUserBody.put("sName", "yavorskiy")
        given().request().body(requestUserBody.toString()).post("/users")

        JSONObject requestAccBody = new JSONObject()
        requestAccBody.put("accBalance", "-1.0")
        expect:
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", get("/users").body().jsonPath().get("[0].userId"))
                .then()
                .statusCode(422)
                .body("balance", comparesEqualTo(new BigDecimal("-1.0")))

        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/users/{id}/accounts creates 2 accounts for concrete user"() {
        setup:
        JSONObject requestUserBody = new JSONObject()
        requestUserBody.put("fName", "ilia")
        requestUserBody.put("sName", "yavorskiy")
        given().request().body(requestUserBody.toString()).post("/users")

        JSONObject requestAccBody = new JSONObject()
        requestAccBody.put("accBalance", "0.0")
        expect:
        def userId = get("/users").body().jsonPath().get("[0].userId")
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", userId)
        .then()
                .statusCode(201)
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", userId)
        .then()
                .statusCode(201)
        when()
                .get("/users")
        .then()
                .statusCode(200)
                .body("[0].accounts.size", equalTo(2))
        delete("/users/{userId}", userId).statusCode() == 200
    }
}

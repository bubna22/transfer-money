package com.bubna.transfer.money.controller.integration

import com.bubna.transfer.money.BaseTestSpec
import io.restassured.path.json.config.JsonPathConfig
import org.jooq.tools.json.JSONObject

import static io.restassured.RestAssured.*
import static io.restassured.config.JsonConfig.jsonConfig
import static io.restassured.config.RestAssuredConfig.newConfig
import static org.hamcrest.Matchers.*

class HttpMoneyControllerSpec extends BaseTestSpec {

    def setup() {
        config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))
    }


    def "/transfers transfers money successfully"() {
        setup:
        JSONObject requestIliaBody = new JSONObject()
        requestIliaBody.put("fName", "ilia")
        requestIliaBody.put("sName", "yavorskiy")
        given().request().body(requestIliaBody.toString()).post("/users")

        JSONObject requestMichaelBody = new JSONObject()
        requestMichaelBody.put("fName", "Michael")
        requestMichaelBody.put("sName", "Smith")
        given().request().body(requestMichaelBody.toString()).post("/users")

        JSONObject requestIliaAccBody = new JSONObject()
        requestIliaAccBody.put("accBalance", "1000.0")

        JSONObject requestMichaelAccBody = new JSONObject()
        requestMichaelAccBody.put("accBalance", "999999.98")

        def userJsonPath = get("/users").body().jsonPath()

        def iliaId = userJsonPath.get("findAll { it.fName == 'ilia' }[0].userId")
        def michaelId = userJsonPath.get("findAll { it.fName == 'Michael' }[0].userId")

        given()
                .request()
                .body(requestIliaAccBody.toString())
                .post("/users/{userId}/accounts", iliaId)

        given()
                .request()
                .body(requestMichaelAccBody.toString())
                .post("/users/{userId}/accounts", michaelId)

        userJsonPath = get("/users").body().jsonPath()
        def iliaAccId = userJsonPath.get("findAll { it.userId == ${iliaId} }[0].accounts[0]")
        def michaelAccId = userJsonPath.get("findAll { it.userId == ${michaelId} }[0].accounts[0]")

        def requestTransferBody = new JSONObject()
        requestTransferBody.put("fr", iliaAccId)
        requestTransferBody.put("to", michaelAccId)
        requestTransferBody.put("amount", "299.50")
        expect:
        given()
                .request()
                .body(requestTransferBody.toString())
                .post("/transfers")
        .then()
                .statusCode(200)

        when()
                .get("/accounts/{accountId}", michaelAccId)
        .then()
                .statusCode(200)
                .body("accBalance", comparesEqualTo(new BigDecimal("999999.98") + new BigDecimal("299.50")))

        when()
                .get("/accounts/{accountId}", iliaAccId)
                .then()
                .statusCode(200)
                .body("accBalance", comparesEqualTo(new BigDecimal("1000.00") - new BigDecimal("299.50")))


        delete("/users/{userId}", iliaId).statusCode() == 200
        delete("/users/{userId}", michaelId).statusCode() == 200
    }

    def "/transfers/:id returns current transfer"() {
        setup:
        JSONObject requestIliaBody = new JSONObject()
        requestIliaBody.put("fName", "ilia")
        requestIliaBody.put("sName", "yavorskiy")
        given().request().body(requestIliaBody.toString()).post("/users")

        JSONObject requestMichaelBody = new JSONObject()
        requestMichaelBody.put("fName", "Michael")
        requestMichaelBody.put("sName", "Smith")
        given().request().body(requestMichaelBody.toString()).post("/users")

        JSONObject requestIliaAccBody = new JSONObject()
        requestIliaAccBody.put("accBalance", "1000.0")

        JSONObject requestMichaelAccBody = new JSONObject()
        requestMichaelAccBody.put("accBalance", "999999.98")

        def userJsonPath = get("/users").body().jsonPath()

        def iliaId = userJsonPath.get("findAll { it.fName == 'ilia' }[0].userId")
        def michaelId = userJsonPath.get("findAll { it.fName == 'Michael' }[0].userId")

        given()
                .request()
                .body(requestIliaAccBody.toString())
                .post("/users/{userId}/accounts", iliaId)

        given()
                .request()
                .body(requestMichaelAccBody.toString())
                .post("/users/{userId}/accounts", michaelId)

        userJsonPath = get("/users").body().jsonPath()
        def iliaAccId = userJsonPath.get("findAll { it.userId == ${iliaId} }[0].accounts[0]")
        def michaelAccId = userJsonPath.get("findAll { it.userId == ${michaelId} }[0].accounts[0]")

        def requestTransferBody = new JSONObject()
        requestTransferBody.put("fr", iliaAccId)
        requestTransferBody.put("to", michaelAccId)
        requestTransferBody.put("amount", "299.50")
        given()
                .request()
                .body(requestTransferBody.toString())
                .post("/transfers")
                .then()
                .statusCode(200)

        delete("/users/{userId}", iliaId).statusCode() == 200
        delete("/users/{userId}", michaelId).statusCode() == 200
        expect:
        when()
                .get("/transfers/{transferId}", get("/transfers").body().jsonPath().get("[0].transferId"))
        .then()
                .statusCode(200)
                .body("transferId", notNullValue())
    }
}

package com.bubna.transfer.money.controller

import com.bubna.transfer.money.service.MoneyService
import com.bubna.transfer.money.utils.ex.impl.NotFoundException
import com.google.gson.Gson
import io.netty.handler.codec.http.DefaultFullHttpRequest
import io.netty.handler.codec.http.HttpMethod
import io.netty.handler.codec.http.HttpVersion
import spock.lang.Specification

import static org.mockito.Mockito.mock

class UnitMoneyControllerSpec extends Specification {

    def "money controller throw not found exception"() {
        setup:
        def controller = new MoneyController(mock(MoneyService.class), new Gson(), null)
        when:
        controller.handle(new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.CONNECT, "/no-such-path"))
        then:
        thrown(NotFoundException.class)
    }
}

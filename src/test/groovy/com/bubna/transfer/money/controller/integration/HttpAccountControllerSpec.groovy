package com.bubna.transfer.money.controller.integration

import com.bubna.transfer.money.BaseTestSpec
import io.restassured.path.json.config.JsonPathConfig
import org.jooq.tools.json.JSONObject

import static io.restassured.RestAssured.*
import static io.restassured.config.JsonConfig.jsonConfig
import static io.restassured.config.RestAssuredConfig.newConfig
import static org.hamcrest.Matchers.comparesEqualTo
import static org.hamcrest.Matchers.equalTo
import static org.hamcrest.Matchers.nullValue

class HttpAccountControllerSpec extends BaseTestSpec {

    def setup() {
        config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))
    }

    def "/accounts/{id} updates account successfully"() {
        setup:
        JSONObject requestUserBody = new JSONObject()
        requestUserBody.put("fName", "ilia")
        requestUserBody.put("sName", "yavorskiy")
        given().request().body(requestUserBody.toString()).post("/users")

        JSONObject requestAccBody = new JSONObject()
        requestAccBody.put("accBalance", "0.0")
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", get("/users").body().jsonPath().get("[0].userId"))
        requestAccBody.put("accBalance", "1000.00")
        def accId = get("/users").body().jsonPath().get("[0].accounts[0]")
        expect:
        given()
                .request()
                .body(requestAccBody.toString())
                .put("/accounts/{id}", accId)

        when()
                .get("/accounts/{id}", accId)
        .then()
                .statusCode(200)
                .body("accBalance", comparesEqualTo(new BigDecimal("1000.0")))
        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/accounts/{id} deletes successfully"() {
        setup:
        JSONObject requestUserBody = new JSONObject()
        requestUserBody.put("fName", "ilia")
        requestUserBody.put("sName", "yavorskiy")
        given().request().body(requestUserBody.toString()).post("/users")

        JSONObject requestAccBody = new JSONObject()
        requestAccBody.put("accBalance", "0.0")
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", get("/users").body().jsonPath().get("[0].userId"))
        requestAccBody.put("accBalance", "1000.00")
        def accId = get("/users").body().jsonPath().get("[0].accounts[0]")
        expect:
        when()
                .delete("/accounts/{id}", accId)
        .then()
                .statusCode(200)

        when()
                .get("/users")
        .then()
                .statusCode(200)
                .body("[0].accounts", nullValue())

        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }

    def "/accounts returns acc"() {
        setup:
        JSONObject requestUserBody = new JSONObject()
        requestUserBody.put("fName", "ilia")
        requestUserBody.put("sName", "yavorskiy")
        given().request().body(requestUserBody.toString()).post("/users")

        JSONObject requestAccBody = new JSONObject()
        requestAccBody.put("accBalance", "0.0")
        given()
                .request()
                .body(requestAccBody.toString())
                .post("/users/{userId}/accounts", get("/users").body().jsonPath().get("[0].userId"))
        requestAccBody.put("accBalance", "1000.00")
        def accId = get("/users").body().jsonPath().get("[0].accounts[0]")
        expect:
        when()
                .get("/accounts")
        .then()
                .statusCode(200)
                .body("[0].accNumber", equalTo(accId))

        delete("/users/{userId}", get("/users").body().jsonPath().get("[0].userId")).statusCode() == 200
    }
}

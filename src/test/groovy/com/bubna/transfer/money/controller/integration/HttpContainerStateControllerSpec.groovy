package com.bubna.transfer.money.controller.integration

import com.bubna.transfer.money.BaseTestSpec
import io.restassured.path.json.config.JsonPathConfig
import org.jooq.tools.json.JSONObject

import static io.restassured.RestAssured.*
import static io.restassured.config.JsonConfig.jsonConfig
import static io.restassured.config.RestAssuredConfig.newConfig
import static org.hamcrest.Matchers.comparesEqualTo
import static org.hamcrest.Matchers.notNullValue

class HttpContainerStateControllerSpec extends BaseTestSpec {

    def setup() {
        config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL))
    }


    def "/health-cheacking returns 404"() {
        expect:
        when()
                .get("/health-checking")
        .then()
                .statusCode(404)
    }
}

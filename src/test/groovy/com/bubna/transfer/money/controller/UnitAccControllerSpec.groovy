package com.bubna.transfer.money.controller

import com.bubna.transfer.money.service.AccountService
import com.bubna.transfer.money.service.UserService
import com.bubna.transfer.money.utils.ex.impl.NotFoundException
import com.google.gson.Gson
import io.netty.handler.codec.http.DefaultFullHttpRequest
import io.netty.handler.codec.http.HttpMethod
import io.netty.handler.codec.http.HttpVersion
import spock.lang.Specification

import static org.mockito.Mockito.mock

class UnitAccControllerSpec extends Specification {

    def "account controller throw not found exception"() {
        setup:
        def controller = new AccountController(mock(AccountService.class), new Gson(), null)
        when:
        controller.handle(new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.CONNECT, "/no-such-path"))
        then:
        thrown(NotFoundException.class)
    }
}

package com.bubna.transfer.money

import com.bubna.transfer.money.http.Server
import com.google.gson.Gson
import spock.lang.Shared
import spock.lang.Specification

import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionStage
import java.util.concurrent.TimeUnit

abstract class BaseTestSpec extends Specification {

    @Shared
    protected CompletableFuture<Void> server

    @Shared
    protected HttpClient httpClient

    @Shared
    protected Gson gson

    def setupSpec() {
        if (server != null) return
        gson = new Gson()
        server = CompletableFuture.runAsync {
            new Server().run(8080)
        }

        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .build()

        HttpRequest healthCheck = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080/health-check/"))
                .GET()
                .build()

        CompletionStage<Void> serverStartedEventFuture = CompletableFuture.runAsync {
            int serverStatus = -1
            while (serverStatus != 200) {
                try {
                    serverStatus = httpClient.send(healthCheck, HttpResponse.BodyHandlers.ofString()).statusCode()
                } catch (Exception ignore) {}
            }
        }.orTimeout(5000, TimeUnit.MILLISECONDS)
        serverStartedEventFuture.join()
        if (serverStartedEventFuture.isCompletedExceptionally() || !serverStartedEventFuture.isDone()) throw new IllegalStateException()
    }

    def cleanupSpec() {
        server.cancel(true)
    }
}

package com.bubna.load // 1

import java.util.concurrent.TimeUnit

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration.FiniteDuration
import scala.util.Random

class MultiThreadingScenario extends Simulation { // 3
  val baseUrl = "http://localhost:8080"
  val contentType = "application/json"
  val createUserEndpoint = "/users"
  val authUser = "blaze"
  val requestCount = 1000

  val feederFirst: Iterator[Map[String, String]] = Iterator.continually(Map("fName" -> Random.alphanumeric.take(20).mkString, "sName" -> Random.alphanumeric.take(20).mkString))
  val feederSecond: Iterator[Map[String, String]] = Iterator.continually(Map("fName2" -> Random.alphanumeric.take(20).mkString, "sName2" -> Random.alphanumeric.take(20).mkString))


  val httpProtocol: HttpProtocolBuilder = http
    .baseUrl(baseUrl)
    .inferHtmlResources()
    .acceptHeader("*/*")
    .contentTypeHeader(contentType)
    .userAgentHeader("curl/7.54.0")


  val scn: ScenarioBuilder = scenario("MultiThreadingScenario")
    .feed(feederFirst)
    .exec(http("user-1-creation")
      .post("/users")
      .body(StringBody("{'fName':'${fName}', 'sName':'${sName}'}"))
      .check(status.is(201))
    ).exec(http("user-1-read")
      .get("/users")
      .check(jsonPath("$[?(@.fName == '${fName}' && @.sName == '${sName}')].userId").saveAs("userFirstId"))
    ).exec(http("user-1-update")
      .put("/users/${userFirstId}")
      .body(StringBody("{'fName':'${fName}', 'sName':'${sName}@foo.com'}"))
      .check(status.is(200))
    ).exec(http("acc-1-creation")
      .post("/users/${userFirstId}/accounts")
      .body(StringBody("{'accBalance':0.0}"))
      .check(status.is(201))
    ).exec(http("acc-1-read")
      .get("/users")
      .check(jsonPath("$[?(@.fName == '${fName}' && @.sName == '${sName}@foo.com')].accounts[0]").saveAs("accFirstId"))
    ).feed(feederSecond)
     .exec(http("user-2-creation")
      .post("/users")
      .body(StringBody("{'fName':'${fName2}', 'sName':'${sName2}'}"))
      .check(status.is(201))
    ).exec(http("user-2-read")
      .get("/users")
      .check(jsonPath("$[?(@.fName == '${fName2}' && @.sName == '${sName2}')].userId").saveAs("userSecondId"))
    ).exec(http("user-2-update")
      .put("/users/${userSecondId}")
      .body(StringBody("{'fName':'${fName2}', 'sName':'${sName2}@foo.com'}"))
      .check(status.is(200))
    ).exec(http("acc-2-creation")
      .post("/users/${userSecondId}/accounts")
      .body(StringBody("{'accBalance':1000.0}"))
      .check(status.is(201))
    ).exec(http("acc-2-read")
      .get("/users")
      .check(jsonPath("$[?(@.fName == '${fName2}' && @.sName == '${sName2}@foo.com')].accounts[0]").saveAs("accSecondId"))
    ).exec(http("transfer")
      .post("/transfers")
      .body(StringBody("{'fr':'${accSecondId}', 'to':'${accFirstId}', 'amount':'500.0'}"))
      .check(status.is(200))
    ).exec(http("user-1-delete")
      .delete("/users/${userFirstId}")
      .check(status.is(200))
    ).exec(http("user-2-delete")
      .delete("/users/${userSecondId}")
      .check(status.is(200))
    )

  setUp(scn.inject(constantUsersPerSec(25) during FiniteDuration.apply(1, TimeUnit.MINUTES))).protocols(httpProtocol)
}